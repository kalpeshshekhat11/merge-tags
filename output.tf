output "tag_Name" {
  description = "The Name tag"
  value = {
    "Name" = local.tag_Name
  }
}

output "tags_default" {
  description = "The default tags"
  value       = var.default_tags
}

output "tags_merged" {
  description = "General use cases, tr:role and Name are not overwritten if provided as add_tags input"
  value       = local.tags_merged
}

output "tags_merged_s3" {
  description = "The tags used to tag S3 resources"
  value       = local.tags_merged_s3
}

output "tags_merged_ec2" {
  description = "The tags used to tag EC2 resources"
  value       = local.tags_merged_ec2
}

output "tags_merged_rds" {
  description = "The tags used to tag RDS resources"
  value       = local.tags_merged_rds
}

output "tags_merged_iam" {
  description = "The tags used to tag IAM resources"
  value       = local.tags_merged_iam
}

output "tags_merged_es" {
  description = "The tags used to tag Elasticsearch resources"
  value       = local.tags_merged_es
}

output "tags_merged_lambda" {
  description = "The tags used to tag Lambda resources"
  value       = local.tags_merged_lambda
}

output "tags_merged_alb" {
  description = "The tags used to tag ALB resources"
  value       = local.tags_merged_alb
}

output "tags_merged_sg" {
  description = "The tags used to tag SG resources"
  value       = local.tags_merged_sg
}

output "tags_merged_efs" {
  description = "The tags used to tag EFS resources"
  value       = local.tags_merged_efs
}

output "tags_merged_kms" {
  description = "The tags used to tag KMS resources"
  value       = local.tags_merged_kms
}

output "tags_merged_asg" {
  description = "The tags used to tag autoscaling groups"
  value = [
    {
      "key"                 = "Name"
      "value"               = local.tags_merged["Name"]
      "propagate_at_launch" = true
    },
    {
      "key"                 = "tr:appFamily"
      "value"               = local.tags_merged["tr:appFamily"]
      "propagate_at_launch" = true
    },
    {
      "key"                 = "tr:appName"
      "value"               = local.tags_merged["tr:appName"]
      "propagate_at_launch" = true
    },
    {
      "key"                 = "tr:environment-type"
      "value"               = local.tags_merged["tr:environment-type"]
      "propagate_at_launch" = true
    },
    {
      "key"                 = "tr:role"
      "value"               = local.tags_merged["tr:role"]
      "propagate_at_launch" = true
    },
    {
      "key"                 = "ca:owner"
      "value"               = local.tags_merged["ca:owner"]
      "propagate_at_launch" = true
    },
    {
      "key"                 = "ca:created-by"
      "value"               = local.tags_merged["ca:created-by"]
      "propagate_at_launch" = true
    },
  ]
}
