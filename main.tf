locals {
  Name = format("%s.%s.%s",
    merge(var.default_tags, var.add_tags)["tr:appName"],
    merge(var.default_tags, var.add_tags)["tr:environment-type"],
    merge(var.default_tags, var.add_tags)["tr:role"],
  )

  tag_Name = lookup(merge(var.default_tags, var.add_tags), "Name", local.Name)

  tags_merged = merge(
    var.default_tags,
    var.add_tags,
    {"Name" = local.tag_Name},
  )

  ###############################################
  ## Overwrites Name & tr:role tags
  ## in order to expose custom merged_tags outputs
  ## for each AWS service.
  ###############################################

  service_map = {
    "S3"     = "s3"
    "ES"     = "es"
    "EC2"    = "node"
    "IAM"    = "iam"
    "RDS"    = "rds"
    "LAMBDA" = "lambda"
    "ALB"    = "alb"
    "SG"     = "sg"
    "EFS"    = "efs"
    "KMS"    = "kms"
  }

  s3_tag_role     = {"tr:role" = local.service_map["S3"]}
  es_tag_role     = {"tr:role" = local.service_map["ES"]}
  ec2_tag_role    = {"tr:role" = local.service_map["EC2"]}
  iam_tag_role    = {"tr:role" = local.service_map["IAM"]}
  rds_tag_role    = {"tr:role" = local.service_map["RDS"]}
  lambda_tag_role = {"tr:role" = local.service_map["LAMBDA"]}
  alb_tag_role    = {"tr:role" = local.service_map["ALB"]}
  sg_tag_role     = {"tr:role" = local.service_map["SG"]}
  efs_tag_role    = {"tr:role" = local.service_map["EFS"]}
  kms_tag_role    = {"tr:role" = local.service_map["KMS"]}

  s3_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.s3_tag_role["tr:role"],
    )
  }

  es_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.es_tag_role["tr:role"],
    )
  }

  ec2_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.ec2_tag_role["tr:role"],
    )
  }

  iam_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.iam_tag_role["tr:role"],
    )
  }

  rds_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.rds_tag_role["tr:role"],
    )
  }

  lambda_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.lambda_tag_role["tr:role"],
    )
  }

  alb_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.alb_tag_role["tr:role"],
    )
  }

  sg_tag_name = {
    "Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.sg_tag_role["tr:role"],
    )
  }

  efs_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.efs_tag_role["tr:role"],
    )
  }

  kms_tag_name = {"Name" = format("%s.%s.%s",
      local.tags_merged["tr:appName"],
      local.tags_merged["tr:environment-type"],
      local.kms_tag_role["tr:role"],
    )
  }

  tags_merged_s3     = merge(local.tags_merged, local.s3_tag_role,     local.s3_tag_name)
  tags_merged_es     = merge(local.tags_merged, local.es_tag_role,     local.es_tag_name)
  tags_merged_ec2    = merge(local.tags_merged, local.ec2_tag_role,    local.ec2_tag_name)
  tags_merged_iam    = merge(local.tags_merged, local.iam_tag_role,    local.iam_tag_name)
  tags_merged_rds    = merge(local.tags_merged, local.rds_tag_role,    local.rds_tag_name)
  tags_merged_lambda = merge(local.tags_merged, local.lambda_tag_role, local.lambda_tag_name)
  tags_merged_alb    = merge(local.tags_merged, local.alb_tag_role,    local.alb_tag_name)
  tags_merged_sg     = merge(local.tags_merged, local.sg_tag_role,     local.sg_tag_name)
  tags_merged_efs    = merge(local.tags_merged, local.efs_tag_role,    local.efs_tag_name)
  tags_merged_kms    = merge(local.tags_merged, local.kms_tag_role,    local.kms_tag_name)
}
