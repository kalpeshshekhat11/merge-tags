# CHANGELOG

## [2.0.3](https://git.clarivate.io/projects/TF/repos/merge-tags/browse?at=refs/tags/2.0.3) (Apr 30, 2020)

- Add tags_merged_kms output value

## [2.0.2](https://git.clarivate.io/projects/TF/repos/merge-tags/browse?at=refs/tags/2.0.2) (Jul 08, 2019)

- Remove `ca:tier` as mandatory tag and add `tr:appFamily` as defaults

## [2.0.1](https://git.clarivate.io/projects/TF/repos/merge-tags/browse?at=refs/tags/2.0.1) (Jun 20, 2019)

- Add tr:appFamily tag to tags_merged_asg output value

## [2.0.0](https://git.clarivate.io/projects/TF/repos/merge-tags/browse?at=refs/tags/2.0.0) (Jun 13, 2019)

- Module refactor to support Terraform [>=0.12](https://www.terraform.io/upgrade-guides/0-12.html)

## [1.0.2](https://git.clarivate.io/projects/TF/repos/merge-tags/browse?at=refs/tags/1.0.2) (Jul 08, 2019)

- Remove `ca:tier` as mandatory tag and add `tr:appFamily` as defaults

## [1.0.1](https://git.clarivate.io/projects/TF/repos/merge-tags/browse?at=refs/tags/1.0.1) (Jun 20, 2019)

- Add tr:appFamily tag to tags_merged_asg output value

## [1.0.0](https://git.clarivate.io/projects/TF/repos/merge-tags/browse?at=refs/tags/1.0.0) (Jun 06, 2019)

- Migration from github to https://git.clarivate.io
