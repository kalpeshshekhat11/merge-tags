variable "default_tags" {
  type        = map(string)
  description = "Default tags"

  default = {
    "tr:appName"          = "NEED TO TAG"
    "tr:environment-type" = "NEED TO TAG"
    "tr:role"             = "NEED TO TAG"
    "tr:appFamily"        = "NEED TO TAG"
    "ca:owner"            = "NEED TO TAG"
    "ca:created-by"       = "terraform"
  }
}

variable "add_tags" {
  type        = map(string)
  description = "Additional tags to assign, provided as input"

  default = {}
}

